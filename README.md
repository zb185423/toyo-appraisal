# toyo-appraisal
## 概要
- 東陽鑑定社の業務支援のためのシステム
- 現状はエクセルで情報管理をしている
- 初回ローンチは、東陽鑑定と特定の保険会社１社での使用を想定

## 業務内容
- 損害保険会社から損害内容の鑑定を依頼される
- 保険の内容に基づき調査（写真、テキスト）
- 書類を作成

## 開発環境
### サーバサイド
- sbt
- JDK 8

### フロント
- npm
- react

## 開発手順
- リポジトリクローン
```sh
git clone git@gitlab.com:zb185423/toyo-appraisal.git
``` 

- ブランチモデル

TODO

- herokuにデプロイ
  - herokuの[アカウント作成](https://devcenter.heroku.com/articles/getting-started-with-scala)
  - 管理者にCollaborators登録依頼
  - Heroku Command Line Interface (CLI)のインストール [手順](https://devcenter.heroku.com/articles/getting-started-with-scala#set-up)
  - [デプロイ](https://devcenter.heroku.com/articles/getting-started-with-scala#deploy-the-app)


